var diametro;

//clasifica los tipos de ruedas en función de su diámetro
function clasificarRueda (diametro) {
    if (diametro <= 10) {
        console.log("La rueda con diámetro " + diametro + ", es una rueda para un juguete pequeño.");
    }
    else if (diametro > 10 && diametro < 20){
        console.log("La rueda con diámetro " + diametro + ", es una rueda para un juguete mediano.");
    }
    else if (diametro >= 20){
        console.log("La rueda con diámetro " + diametro + ", es una rueda para un juguete grande.");
    }
}

clasificarRueda(5);
clasificarRueda(10);
clasificarRueda(15);
clasificarRueda(20);
clasificarRueda(25);